﻿using System;

namespace ex_16
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #16 - Playing with Spaces
            // --------------------------------------------
            // By using certain characters repeatedly in a string, it is possible to create a bit of a look to your app that is more than text.
            // For example by using ***** or by using ====== inside a string.
            // Where is becomes challenging is when we need to display data in that same line.   
            //
            // *******************************
            // ****   Welcome to my app   ****
            // *******************************
            //
            // *******************************
            // What is your name?
            //
            // Your name is:
            // Create a new project (including repository and the dotnet commands and np snippet)
            // Reproduce the menu you see above in code and make it part of your application.

            // Start the program with Clear();
            Console.Clear();
      
            // Print out my name
            var myName = "Sing Graajae Cho (Shinkee Cho)";
            Console.WriteLine("*******************************");
            Console.WriteLine("      Welcome to NewShop\n");
            Console.WriteLine("*******************************");
            Console.WriteLine($"My name is {myName}.\n");
        }
    }
}
